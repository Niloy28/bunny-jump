using BunnyJump.Player;
using UnityEngine;

namespace BunnyJump.SceneManagement
{
    public class SceneTransitionHandler : MonoBehaviour
    {
        private Animator _animator;

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
            _animator = GetComponent<Animator>();
        }

        private void SetToCurrentPlayerPosition()
        {
            var player = FindObjectOfType<PlayerMovement>();
            if (player != null)
            {
                transform.position = new Vector3(player.transform.position.x, player.transform.position.y, transform.position.z);
            }
        }

        public void CircleFadeIn()
        {
            _animator.SetTrigger("fadeIn");

        }

        public void CircleFadeOut()
        {
            _animator.SetTrigger("fadeOut");
        }

        public void OnSceneTransitionEnded()
        {
            // on fade out done -> trigger new scene load
            SceneEvent.FireSceneTransitionEnded();
        }

        private void OnEnable()
        {
            SceneEvent.SceneChangeTriggered += CircleFadeOut;
            SceneEvent.SceneChangeEnded += CircleFadeIn;
        }

        private void OnDisable()
        {
            SceneEvent.SceneChangeTriggered -= CircleFadeOut;
            SceneEvent.SceneChangeEnded -= CircleFadeIn;
        }
    }
}