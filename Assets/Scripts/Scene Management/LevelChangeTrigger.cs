using UnityEngine;

namespace BunnyJump.SceneManagement
{
    public class LevelChangeTrigger : MonoBehaviour
    {
        [SerializeField] private int targetLevel;

        private void OnTriggerEnter2D(Collider2D other)
        {
            SceneLoader.Instance.LoadLevel(targetLevel);
        }
    }
}