using BunnyJump.Player;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BunnyJump.SceneManagement
{
    public class SceneLoader : MonoBehaviour
    {
        public static SceneLoader Instance { get; private set; }

        [SerializeField] private SceneTransitionHandler transitionHandler;

        private int _currentSceneIndex = 0;
        private int _unloadIndex;

        private void ImplementSingleton()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void OnSceneTransitionEnded()
        {
            int sceneToLoad = Mathf.Clamp(_currentSceneIndex, 0, SceneManager.sceneCountInBuildSettings);

            SceneManager.UnloadSceneAsync(_unloadIndex);
            SceneManager.LoadScene(sceneToLoad, LoadSceneMode.Additive);

            // on scene loading done -> fade in to game
            SceneEvent.FireSceneChangeEnded();
        }

        public void ResumeLevel()
        {
            SceneManager.UnloadSceneAsync("Pause Menu");
            Time.timeScale = 1.0f;
        }

        public void PauseLevel()
        {
            Time.timeScale = 0f;
            SceneManager.LoadScene("Pause Menu", LoadSceneMode.Additive);
        }

        private void OnPauseMenuToggled()
        {
            if (SceneManager.GetSceneByName("Pause Menu").isLoaded)
            {
                ResumeLevel();
            }
            else
            {
                PauseLevel();
            }
        }

        public void LoadLevel(int level)
        {
            _unloadIndex = _currentSceneIndex;
            _currentSceneIndex = level;
            SceneEvent.FireSceneChangedTriggered();
        }

        public void ReloadLevel()
        {
            _unloadIndex = _currentSceneIndex;
            if (SceneManager.GetSceneByName("Game Over").isLoaded) SceneManager.UnloadSceneAsync("Game Over");
            if (SceneManager.GetSceneByName("Pause Menu").isLoaded) SceneManager.UnloadSceneAsync("Pause Menu");

            Time.timeScale = 1f;
            SceneEvent.FireSceneChangedTriggered();
            PlayerHealthManager.Instance.ResetHealth();
        }

        public void LoadTitleScreen()
        {
            _unloadIndex = _currentSceneIndex;
            _currentSceneIndex = 0;
            if (SceneManager.GetSceneByName("Game Over").isLoaded) SceneManager.UnloadSceneAsync("Game Over");
            if (SceneManager.GetSceneByName("Pause Menu").isLoaded) SceneManager.UnloadSceneAsync("Pause Menu");

            Time.timeScale = 1f;
            SceneEvent.FireSceneChangedTriggered();
            PlayerHealthManager.Instance.ResetHealth();
        }

        public void LoadGameOver()
        {
            SceneManager.LoadScene("Game Over", LoadSceneMode.Additive);
        }

        private void OnEnable()
        {
            ImplementSingleton();
            SceneEvent.NewLevelRequested += LoadLevel;
            SceneEvent.SceneTransitionEnded += OnSceneTransitionEnded;
            SceneEvent.LevelPauseToggled += OnPauseMenuToggled;
            SceneManager.sceneLoaded += (scene, loadMode) => SceneManager.SetActiveScene(scene);
        }

        private void OnDisable()
        {
            SceneEvent.SceneTransitionEnded -= OnSceneTransitionEnded;
            SceneEvent.LevelPauseToggled -= OnPauseMenuToggled;
            SceneManager.sceneLoaded -= (scene, loadMode) => SceneManager.SetActiveScene(scene);
        }
    }
}