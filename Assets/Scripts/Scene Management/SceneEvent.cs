using System;

namespace BunnyJump.SceneManagement
{
    public static class SceneEvent
    {
        public static event Action<int> NewLevelRequested;
        public static event Action SceneChangeTriggered;
        public static event Action SceneTransitionEnded;
        public static event Action SceneChangeEnded;
        public static event Action LevelPauseToggled;

        public static void FireNewLevelRequested(int level)
        {
            NewLevelRequested?.Invoke(level);
        }

        public static void FireSceneChangedTriggered()
        {
            SceneChangeTriggered?.Invoke();
        }

        public static void FireSceneChangeEnded()
        {
            SceneChangeEnded?.Invoke();
        }

        public static void FireSceneTransitionEnded()
        {
            SceneTransitionEnded?.Invoke();
        }

        public static void FireLevelPauseToggleEvent()
        {
            LevelPauseToggled?.Invoke();
        }
    }
}

