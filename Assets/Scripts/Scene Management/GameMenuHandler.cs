using UnityEngine;

namespace BunnyJump.SceneManagement
{
    public class GameMenuHandler : MonoBehaviour
    {
        public void OnStartGameClicked()
        {
            SceneEvent.FireNewLevelRequested(1);
        }

        public void OnResumeButtonClicked()
        {
            SceneLoader.Instance.ResumeLevel();
        }

        public void OnOptionsButtonClicked()
        {
            Debug.Log("Options");
        }

        public void OnQuitGameClicked()
        {
            Application.Quit();
            Debug.Log("Quit Game.");
        }

        public void OnTitleScreenButtonClicked()
        {
            SceneLoader.Instance.LoadTitleScreen();
        }

        public void OnTryAgainClicked()
        {
            SceneLoader.Instance.ReloadLevel();
        }
    }
}