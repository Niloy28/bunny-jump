using UnityEngine;
using System.Collections;

namespace BunnyJump.Enemy
{
    public class EnemyHealth : MonoBehaviour, IHealth
    {
        [SerializeField] private int maxHealth = 3;
        [SerializeField] private float invulTime = 1.5f;
        [SerializeField] private int flashCount = 5;

        private SpriteRenderer _spriteRenderer;
        private Animator _animator;
        private int _health;
        private bool _isInvulnerable;

        private void Start()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _animator = GetComponent<Animator>();
            _health = maxHealth;
        }

        public void TakeDamage(int damageAmount)
        {
            if (!_isInvulnerable)
            {
                _health--;
                if (_health <= 0)
                {
                    _animator.SetTrigger("death");
                    Destroy(gameObject, 0.25f);
                }
                else
                {
                    StartCoroutine(StartInvulTimer());
                }
            }
        }

        private IEnumerator StartInvulTimer()
        {
            _isInvulnerable = true;

            float flashDuration = invulTime / (flashCount * 2);
            WaitForSeconds flashWait = new WaitForSeconds(flashDuration);

            for (int i = 0; i < 5; i++)
            {
                _spriteRenderer.enabled = false;
                yield return flashWait;
                _spriteRenderer.enabled = true;
                yield return flashWait;
            }
            _isInvulnerable = false;
        }
    }
}