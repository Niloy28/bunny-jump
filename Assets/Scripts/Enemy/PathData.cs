using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BunnyJump.Enemy
{
    [System.Serializable]
    public struct PathData
    {
        [SerializeField] private List<Transform> pathNodes;

        public List<Transform> PathNodes => pathNodes;
    }
}