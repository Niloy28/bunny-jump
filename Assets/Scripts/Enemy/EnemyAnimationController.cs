using UnityEngine;

namespace BunnyJump.Enemy
{
    public class EnemyAnimationController : MonoBehaviour
    {
        private Animator _animator;
        private SpriteRenderer _spriteRenderer;

        private void Start()
        {
            _animator = GetComponent<Animator>();
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        public void AnimateMovement(int velocity)
        {
            _animator.SetInteger("velocity", Mathf.Abs(velocity));

            if (velocity < 0) _spriteRenderer.flipX = true;
            else if (velocity > 0) _spriteRenderer.flipX = false;
        }
    }
}