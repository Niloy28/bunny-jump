using UnityEngine;

namespace BunnyJump.Enemy
{
    public class EnemyMovement : MonoBehaviour
    {
        [SerializeField] private PathData pathData;
        [SerializeField] private float moveSpeed = 7f;
        [SerializeField] private float idleTime = 1f;

        private Transform _currentPos;
        private Transform _currentTarget;
        private EnemyAnimationController _animationController;
        private float _idleTimer;
        private int _targetPathNodeIndex;

        private void Start()
        {
            _animationController = GetComponent<EnemyAnimationController>();
            SetPathData();
        }

        private void SetPathData()
        {
            _currentPos = pathData.PathNodes[0];
            _currentTarget = pathData.PathNodes[1];
            _targetPathNodeIndex = 1;

            transform.position = _currentPos.position;
        }

        private void Update()
        {
            if (IsIdle())
            {
                _idleTimer -= Time.deltaTime;
                return;
            }

            MoveTowardsTarget();

            if (HasReachedTarget())
            {
                SetNewTarget();
            }
        }

        private void SetNewTarget()
        {
            _idleTimer = idleTime;
            _targetPathNodeIndex++;
            if (_targetPathNodeIndex >= pathData.PathNodes.Capacity)
            {
                _targetPathNodeIndex = 0;
            }
        }

        private bool HasReachedTarget()
        {
            return Vector2.Distance(transform.position, _currentTarget.position) <= 0.01f;
        }

        private void MoveTowardsTarget()
        {
            _currentTarget = pathData.PathNodes[_targetPathNodeIndex];
            transform.position = Vector2.MoveTowards(transform.position, _currentTarget.position, Time.deltaTime * moveSpeed);

            int velocity = Mathf.FloorToInt(_currentTarget.position.x - transform.position.x);
            _animationController.AnimateMovement(velocity);
        }

        private bool IsIdle()
        {
            return _idleTimer > 0.01f;
        }
    }
}