using System.Collections.Generic;
using UnityEngine;

namespace BunnyJump.Audio
{
    [CreateAssetMenu(menuName = "Audio/New Music Data", fileName = "New Music Data")]
    public class MusicDataObject : ScriptableObject
    {
        [SerializeField] private List<AudioClip> musicClips;

        public List<AudioClip> MusicClips => musicClips;
    }
}