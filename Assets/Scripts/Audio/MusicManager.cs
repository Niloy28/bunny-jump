using UnityEngine;
using UnityEngine.SceneManagement;
using BunnyJump.SceneManagement;

namespace BunnyJump.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class MusicManager : MonoBehaviour
    {
        public static MusicManager Instance { get; private set; }

        private MusicDataObject _musicDataObject;
        private AudioSource _audioSource;

        private void Awake()
        {
            ImplementSingleton();
        }

        private void Start()
        {
            _musicDataObject = Resources.Load<MusicDataObject>("Music/Level Music");
            _audioSource = GetComponent<AudioSource>();
        }

        private void ImplementSingleton()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void ChangeMusic()
        {
            int currScene = SceneManager.GetActiveScene().buildIndex;

            _audioSource.Stop();
            _audioSource.clip = _musicDataObject.MusicClips[currScene];
            _audioSource.Play();
        }

        private void OnEnable()
        {
            SceneEvent.SceneChangeEnded += ChangeMusic;
        }

        private void OnDisable()
        {
            SceneEvent.SceneChangeEnded -= ChangeMusic;
        }
    }
}