using UnityEngine;
using BunnyJump.Player;

public class GunPickup : MonoBehaviour, IPickup
{
    public void OnPickup()
    {
        throw new System.NotImplementedException();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerAction>().OnGunPickedUp();
            Destroy(gameObject);
        }
    }
}
