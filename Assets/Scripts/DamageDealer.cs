using UnityEngine;

public class DamageDealer : MonoBehaviour
{
    [SerializeField] private string damageReceiverTag;
    [SerializeField] private int damageAmount;
    [SerializeField] private bool destroyOnHit;

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.CompareTag(damageReceiverTag))
        {
            other.collider.GetComponent<IHealth>().TakeDamage(damageAmount);
        }
        if (destroyOnHit) Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(damageReceiverTag))
        {
            other.GetComponent<IHealth>().TakeDamage(damageAmount);

            if (destroyOnHit) Destroy(gameObject);
        }
    }
}
