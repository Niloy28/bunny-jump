using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using BunnyJump.Player;

namespace BunnyJump.UI
{
    public class HealthDisplay : MonoBehaviour
    {
        [SerializeField] private GameObject heartDisplayPrefab;
        [SerializeField] private Sprite fullHeart;
        [SerializeField] private Sprite halfHeart;
        [SerializeField] private Sprite emptyHeart;

        private List<GameObject> _heartDisplays;
        private int _currentHealthPointer;
        private bool _isHealthDisplayLoaded;

        private void LoadHealthDisplay()
        {
            if (!_isHealthDisplayLoaded)
            {
                float maxHearts = PlayerHealthManager.Instance.MaxHealth / 2f;
                int fullHearts = Mathf.FloorToInt(maxHearts);

                for (int i = 0; i < fullHearts; i++)
                {
                    var heartDisplay = Instantiate(heartDisplayPrefab, transform);
                    _heartDisplays.Add(heartDisplay);
                }
                // point to last heart display item for updates
                _currentHealthPointer = _heartDisplays.Count - 1;

                _isHealthDisplayLoaded = true;
            }
        }

        public void UpdateHealthDisplay(bool increaseHealth)
        {
            var currentLastHeartGO = _heartDisplays[_currentHealthPointer];
            if (currentLastHeartGO == null) return;

            var currentLastHeart = currentLastHeartGO.GetComponent<Image>();

            if (increaseHealth)
            {
                IncreaseHeart(currentLastHeart);
            }
            else
            {
                DecreaseHeart(currentLastHeart);
            }
        }

        private void IncreaseHeart(Image currentLastHeart)
        {
            if (currentLastHeart.sprite == halfHeart)
            {
                // replace half with full heart and increment counter
                currentLastHeart.sprite = fullHeart;
                _currentHealthPointer = Mathf.Clamp(++_currentHealthPointer, 0, _heartDisplays.Capacity - 1);
            }
            else if (currentLastHeart.sprite == fullHeart)
            {
                // increment pointer and add a new half heart
                _currentHealthPointer = Mathf.Clamp(++_currentHealthPointer, 0, _heartDisplays.Capacity - 1);
                _heartDisplays[_currentHealthPointer].GetComponent<Image>().sprite = halfHeart;
            }
        }

        private void DecreaseHeart(Image currentLastHeart)
        {
            if (currentLastHeart.sprite == halfHeart)
            {
                // empty half heart and decrement counter
                currentLastHeart.sprite = emptyHeart;
                _currentHealthPointer = Mathf.Clamp(--_currentHealthPointer, 0, _heartDisplays.Capacity - 1);
            }
            else if (currentLastHeart.sprite == fullHeart)
            {
                // replace full with half heart
                currentLastHeart.sprite = halfHeart;
            }
        }

        private void ClearHealthDisplay()
        {
            if (_isHealthDisplayLoaded)
            {
                foreach (var heartDisplay in _heartDisplays)
                {
                    Destroy(heartDisplay);
                }
                _isHealthDisplayLoaded = false;
            }
        }

        public void ResetHealthDisplay()
        {
            ClearHealthDisplay();
            LoadHealthDisplay();
        }

        private void OnNewSceneLoaded(Scene scene, LoadSceneMode _)
        {
            // clear health ui if main menu scene
            if (scene.buildIndex == 0)
            {
                ClearHealthDisplay();
            }
            else
            {
                LoadHealthDisplay();
            }
        }

        private void OnEnable()
        {
            _heartDisplays = new List<GameObject>();
            SceneManager.sceneLoaded += OnNewSceneLoaded;
        }

        private void OnDisable()
        {
            SceneManager.sceneLoaded -= OnNewSceneLoaded;
        }
    }
}