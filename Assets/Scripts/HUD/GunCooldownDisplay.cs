using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using BunnyJump.Player;

namespace BunnyJump.UI
{
    public class GunCooldownDisplay : MonoBehaviour
    {
        [SerializeField] private Slider cooldownSlider;

        private void OnSliderValueChanged(float value)
        {
            cooldownSlider.value = value;
        }

        private void OnNewSceneLoaded(Scene _, LoadSceneMode __)
        {
            cooldownSlider.gameObject.SetActive(false);
        }

        private void OnWeaponEquipped()
        {
            cooldownSlider.gameObject.SetActive(true);
        }

        private void OnEnable()
        {
            PlayerAction.CooldownValueChanged += OnSliderValueChanged;
            PlayerAction.WeaponEquipped += OnWeaponEquipped;
            SceneManager.sceneLoaded += OnNewSceneLoaded;
        }

        private void OnDisable()
        {
            PlayerAction.CooldownValueChanged -= OnSliderValueChanged;
            PlayerAction.WeaponEquipped -= OnWeaponEquipped;
            SceneManager.sceneLoaded -= OnNewSceneLoaded;
        }
    }
}