using System;
using System.Collections;
using UnityEngine;

namespace BunnyJump.Player
{
    public class PlayerHealth : MonoBehaviour, IHealth
    {
        public static event Action<int> PlayerDamageReceived;

        [SerializeField] private float damageRecoil = 5f;
        [SerializeField] private float invulTime = 1.5f;
        [SerializeField] private int flashCount = 5;

        private SpriteRenderer _spriteRenderer;
        private Rigidbody2D _rb;
        private bool _isInvulnerable;

        private void Start()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _rb = GetComponent<Rigidbody2D>();
        }

        public void TakeDamage(int damageAmount)
        {
            if (!_isInvulnerable)
            {
                // detect direction, default sprite is right faced
                bool isRightFacing = _spriteRenderer.flipX;

                // BUG: recoil not working
                var recoilDirection = isRightFacing ? Vector2.right : Vector2.left;
                Vector2 recoil = recoilDirection * damageRecoil;
                _rb.AddForce(recoil, ForceMode2D.Impulse);

                PlayerDamageReceived?.Invoke(damageAmount);
                if (PlayerHealthManager.Instance.Health > 0)
                    StartCoroutine(StartInvulTimer());
            }
        }

        private IEnumerator StartInvulTimer()
        {
            _isInvulnerable = true;

            float flashDuration = invulTime / (flashCount * 2);
            WaitForSeconds flashWait = new WaitForSeconds(flashDuration);

            for (int i = 0; i < 5; i++)
            {
                _spriteRenderer.enabled = false;
                yield return flashWait;
                _spriteRenderer.enabled = true;
                yield return flashWait;
            }
            _isInvulnerable = false;
        }
    }
}