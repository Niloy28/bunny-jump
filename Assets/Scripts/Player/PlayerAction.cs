using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace BunnyJump.Player
{
    public class PlayerAction : MonoBehaviour
    {
        public static event Action<float> CooldownValueChanged;
        public static event Action WeaponEquipped;

        [SerializeField] private float shootCooldown = 0.5f;
        [SerializeField] private GameObject bulletPrefab;
        [SerializeField] private Transform leftBulletSpawnPoint;
        [SerializeField] private Transform rightBulletSpawnPoint;

        private PlayerAnimationController _animationController;
        private SpriteRenderer _spriteRenderer;
        private float _shootTimer;
        private bool _isWeaponEquipped;

        private void Start()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _animationController = GetComponent<PlayerAnimationController>();
        }

        private void Update()
        {
            if (_isWeaponEquipped)
            {
                _shootTimer += Time.deltaTime;
                _shootTimer = Mathf.Clamp(_shootTimer, 0, shootCooldown);
                CooldownValueChanged?.Invoke(_shootTimer);
            }
        }

        public void OnGunPickedUp()
        {
            _isWeaponEquipped = true;
            _shootTimer = shootCooldown;
            WeaponEquipped?.Invoke();
            _animationController.OnWeaponEquipped();
        }

        private void OnShoot(InputAction.CallbackContext ctx)
        {
            if (_isWeaponEquipped && ctx.performed && _shootTimer >= shootCooldown)
            {
                _animationController.AnimateShoot();
                ShootBullet();
                _shootTimer = 0;
            }
        }

        private void ShootBullet()
        {
            var bulletSpawnPoint = _spriteRenderer.flipX ? leftBulletSpawnPoint : rightBulletSpawnPoint;
            var bullet = Instantiate(bulletPrefab, bulletSpawnPoint.position, Quaternion.identity);
            Vector2 bulletDirection = _spriteRenderer.flipX ? Vector2.left : Vector2.right;

            bullet.GetComponent<SpriteRenderer>().flipX = _spriteRenderer.flipX;
            bullet.GetComponent<Rigidbody2D>().velocity = 10f * bulletDirection;
            Destroy(bullet, 5f);
        }

        private void OnEnable()
        {
            PlayerInputEvent.PlayerShoot += OnShoot;
        }

        private void OnDisable()
        {
            PlayerInputEvent.PlayerShoot -= OnShoot;
        }
    }
}