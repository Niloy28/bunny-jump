using UnityEngine;
using UnityEngine.InputSystem;
using BunnyJump.SceneManagement;

namespace BunnyJump.Player
{
    public class PlayerInputManager : MonoBehaviour
    {
        public static PlayerInputManager Instance { get; private set; }

        [SerializeField] private PlayerInput playerInput;
        [SerializeField] private float debounceTime = 0.1f;

        // used to debounce same input key repeatations 
        private float _debounceTimer = 0f;

        private void OnEnable()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Update()
        {
            _debounceTimer -= Time.unscaledDeltaTime;
        }

        public void OnPlayerMovement(InputAction.CallbackContext ctx)
        {
            PlayerInputEvent.FirePlayerMovementEvent(ctx);
        }

        public void OnPlayerJump(InputAction.CallbackContext ctx)
        {
            PlayerInputEvent.FirePlayerJumpEvent(ctx);
        }

        public void OnPlayerShoot(InputAction.CallbackContext ctx)
        {
            PlayerInputEvent.FirePlayerShootEvent(ctx);
        }

        public void OnGamePauseToggled()
        {
            Debug.Log(_debounceTimer);
            if (_debounceTimer <= 0)
            {
                SceneEvent.FireLevelPauseToggleEvent();
                _debounceTimer = debounceTime;
            }
        }

        public void DisablePlayerInput()
        {
            playerInput.DeactivateInput();
        }

        public void EnablePlayerInput()
        {
            playerInput.ActivateInput();
        }
    }
}