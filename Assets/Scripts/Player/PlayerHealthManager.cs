using UnityEngine;
using BunnyJump.UI;
using BunnyJump.SceneManagement;

namespace BunnyJump.Player
{
    public class PlayerHealthManager : MonoBehaviour
    {
        public static PlayerHealthManager Instance { get; private set; }

        public int Health => _health;
        public int MaxHealth => maxHealth;

        [SerializeField] private HealthDisplay healthDisplay;
        [SerializeField] private int maxHealth = 4;

        private int _health;

        private void Awake()
        {
            ImplementSingleton();
            _health = maxHealth;

        }

        private void ImplementSingleton()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void OnHealthPickup(int healAmount)
        {
            _health += healAmount;
            healthDisplay.UpdateHealthDisplay(increaseHealth: true);
        }

        public void OnPlayerDamageReceived(int damageAmount)
        {
            _health -= damageAmount;
            healthDisplay.UpdateHealthDisplay(increaseHealth: false);

            if (_health <= 0)
            {
                // initiate game over sequence
                PlayerInputManager.Instance.DisablePlayerInput();
                Time.timeScale = 0f;
                SceneLoader.Instance.LoadGameOver();
            }
        }

        public void ResetHealth()
        {
            _health = maxHealth;
            healthDisplay.ResetHealthDisplay();
            PlayerInputManager.Instance.EnablePlayerInput();
        }

        private void OnEnable()
        {
            PlayerHealth.PlayerDamageReceived += OnPlayerDamageReceived;
        }

        private void OnDisable()
        {
            PlayerHealth.PlayerDamageReceived -= OnPlayerDamageReceived;
        }
    }
}