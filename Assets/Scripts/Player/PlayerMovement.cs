using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace BunnyJump.Player
{
    public class PlayerMovement : MonoBehaviour
    {
        [Range(5f, 15f)]
        [SerializeField] private float movementSpeed = 9f;
        [Range(20f, 50f)]
        [SerializeField] private float jumpForce = 25f;
        [SerializeField] private Transform playerFeet;
        [SerializeField] private ParticleSystem dustEffect;
        [SerializeField] private LayerMask groundMask;

        private PlayerAnimationController _animationController;
        private Rigidbody2D _rb;
        private float _moveDirection;
        private bool _isGrounded;
        private float _circleRadius = 0.3f;

        private void Start()
        {
            _animationController = GetComponent<PlayerAnimationController>();
            _rb = GetComponent<Rigidbody2D>();
        }

        private void Update()
        {
            _animationController.AnimateMovement(_moveDirection);
            _animationController.AnimateJump(_isGrounded);
        }

        private void FixedUpdate()
        {
            _rb.velocity = new Vector2(_moveDirection * movementSpeed, _rb.velocity.y);
            _isGrounded = Physics2D.OverlapCircle(playerFeet.position, _circleRadius, groundMask);
        }

        private void OnMovement(InputAction.CallbackContext ctx)
        {
            _moveDirection = ctx.ReadValue<float>();

            if (ctx.performed) dustEffect.Play();
        }

        private void OnJump(InputAction.CallbackContext ctx)
        {
            if (_isGrounded && ctx.performed)
            {
                _rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
                dustEffect.Play();
            }
        }

        private void OnEnable()
        {
            PlayerInputEvent.PlayerMovement += OnMovement;
            PlayerInputEvent.PlayerJump += OnJump;
        }

        private void OnDisable()
        {
            PlayerInputEvent.PlayerMovement -= OnMovement;
            PlayerInputEvent.PlayerJump -= OnJump;
        }
    }
}