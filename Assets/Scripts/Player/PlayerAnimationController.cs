using UnityEngine;

namespace BunnyJump.Player
{
    public class PlayerAnimationController : MonoBehaviour
    {
        private Animator _animator;
        private SpriteRenderer _spriteRenderer;

        private void Start()
        {
            _animator = GetComponent<Animator>();
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        public void AnimateMovement(float moveDirection)
        {
            _animator.SetFloat("velocity", Mathf.Abs(moveDirection));

            if (moveDirection < 0) _spriteRenderer.flipX = true;
            if (moveDirection > 0) _spriteRenderer.flipX = false;
        }

        public void AnimateJump(bool grounded)
        {
            _animator.SetBool("grounded", grounded);
        }

        public void OnWeaponEquipped()
        {
            _animator.SetLayerWeight(_animator.GetLayerIndex("Equipped"), 1f);
        }

        public void AnimateShoot()
        {
            _animator.SetTrigger("shoot");
        }
    }
}