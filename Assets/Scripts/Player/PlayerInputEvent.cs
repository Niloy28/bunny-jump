using System;
using UnityEngine.InputSystem;

public static class PlayerInputEvent
{
    public static event Action<InputAction.CallbackContext> PlayerMovement;
    public static event Action<InputAction.CallbackContext> PlayerJump;
    public static event Action<InputAction.CallbackContext> PlayerShoot;

    public static void FirePlayerMovementEvent(InputAction.CallbackContext ctx)
    {
        PlayerMovement?.Invoke(ctx);
    }

    public static void FirePlayerJumpEvent(InputAction.CallbackContext ctx)
    {
        PlayerJump?.Invoke(ctx);
    }

    public static void FirePlayerShootEvent(InputAction.CallbackContext ctx)
    {
        PlayerShoot?.Invoke(ctx);
    }
}
