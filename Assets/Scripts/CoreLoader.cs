using UnityEngine;
using UnityEngine.SceneManagement;

public class CoreLoader : MonoBehaviour
{
    private void OnEnable()
    {
        // load core scene if not loaded
        if (!SceneManager.GetSceneByName("Core").IsValid())
        {
            SceneManager.LoadScene("Core", LoadSceneMode.Additive);
        }
    }
}
