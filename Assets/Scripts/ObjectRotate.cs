using UnityEngine;

public class ObjectRotate : MonoBehaviour
{
    [SerializeField] private float rotateSpeed = 50f;

    private void Update()
    {
        transform.rotation *= Quaternion.AngleAxis(Time.deltaTime * rotateSpeed, Vector3.up);
    }
}
